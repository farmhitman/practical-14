#include <iostream>
#include <string>

int main()
{
    std::string mStr = "Homework";
    int mLength = mStr.length();
    printf("Length = %d\nFirst char = %c\nLast char = %c\n", mLength, mStr[0], mStr[mLength - 1]);
}
